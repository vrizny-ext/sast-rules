from django.db.models.expressions import RawSQL
from django.http import HttpResponse

def get_user_age(request):
  # ruleid: python_django_rule-django-rawsql-used
  user_name = request.get('user_name')
  user_age = RawSQL('SELECT user_age FROM myapp_person where user_name = %s' % user_name)
  html = "<html><body>User Age %s.</body></html>" % user_age
  return HttpResponse(html)

def get_user_age(request):
  # ruleid: python_django_rule-django-rawsql-used
  user_name = request.get('user_name')
  user_age = RawSQL(f'SELECT user_age FROM myapp_person where user_name = {user_name}')
  html = "<html><body>User Age %s.</body></html>" % user_age
  return HttpResponse(html)

def get_user_age(request):
  # ruleid: python_django_rule-django-rawsql-used
  user_name = request.get('user_name')
  user_age = RawSQL('SELECT user_age FROM myapp_person where user_name = %s'.format(user_name))
  html = "<html><body>User Age %s.</body></html>" % user_age
  return HttpResponse(html)

def get_users(request):
  # ruleid: python_django_rule-django-rawsql-used
  client_id = request.headers.get('client_id')
  users = RawSQL('SELECT * FROM myapp_person where client_id = %s'.format(client_id))
  html = "<html><body>Users %s.</body></html>" % users
  return HttpResponse(html)

def get_users(request):
  # ruleid: python_django_rule-django-rawsql-used
  client_id = request.headers.get('client_id')
  users = RawSQL(f'SELECT * FROM myapp_person where client_id = {client_id}')
  html = "<html><body>Users %s.</body></html>" % users
  return HttpResponse(html)

def get_users(request):
  client_id = request.headers.get('client_id')
  # ruleid: python_django_rule-django-rawsql-used
  users = RawSQL('SELECT * FROM myapp_person where client_id = "%s"', (client_id,))
  html = "<html><body>Users %s.</body></html>" % users
  return HttpResponse(html)

def get_users(request):
  client_id = request.headers.get('client_id')
  # ok: python_django_rule-django-rawsql-used
  users = RawSQL('SELECT * FROM myapp_person where client_id = %s', (client_id,))
  html = "<html><body>Users %s.</body></html>" % users
  return HttpResponse(html)